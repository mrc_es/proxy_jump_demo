#!/usr/bin/env bash

#[IMPORTANT] Cambiar puerto al mapeo adecuado
#[START Config]
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin # For cronjob environment
DIRNAME=$( dirname "$( realpath -s "$0" )" )

REMOTE_USER="bastion"
REMOTE_HOST="<ip bastion>"
REMOTE_NODE="$REMOTE_USER@$REMOTE_HOST"

REMOTE_PORT=10001
LOCAL_PORT=19922

SSH_FOLDER="/home/juan-remoto/.ssh"

PACKED_PRIVATE_KEY_FILE="$DIRNAME/credentials/bastion_id_rsa"
PACKED_PUBLIC_KEY_FILE="$DIRNAME/credentials/bastion_id_rsa.pub"
#[END Config]

check_key() {
: << __description__

    We just perform simply ad hoc operations to verify the state of the key pairs.

__description__

    mkdir -p "$SSH_FOLDER"
    touch "$SSH_FOLDER/authorized_keys"
    
    cat "$SSH_FOLDER/authorized_keys" "$PACKED_PUBLIC_KEY_FILE" \
        | awk '!seen[$0]++' \
        | tee "$SSH_FOLDER/authorized_keys" > /dev/null
    
    [ "$(stat -c "%a" "$PACKED_PRIVATE_KEY_FILE")" != "600" ] \
        && chmod 600 "$PACKED_PRIVATE_KEY_FILE"
}

check_and_create_tunnel() {
: << __description__

    We execute a simple netstat in order to test our connection with the bastion/node

    ssh parameters:
    
    * -i PACKED_PRIVATE_KEY_FILE -> Specifies the rsa_id private file location

    * -o UserKnownHostsFile=/dev/null -> For avoiding to confirm the remote host identity key

    * -o StrictHostKeyChecking=no -> ssh will automatically add new host keys to the user known hosts
                                     files and allow connections to hosts with changed hostkeys to proceed

    * REMOTE_USER@127.0.0.1 -> In case we have established a connection with the bastion in the past,
                               this connection should be accessible to the REMOTE_USER through our
                               localhost (127.0.0.1). But the connection is not mandatory

    * -p LOCAL_PORT -> In case we've established a connection before, this should be running
                       in the port LOCAL_PORT

    * netstat... -> Is a command line only for get the exit status of the ssh command.
                    we can change it for whatever. We're just trying to test the connection
                    with the bastion in the first strep

    If dont return a succesful status, we proceed to create the connection with the bastion
__description__

    # We check the status of the connection
    ssh \
        -i "$PACKED_PRIVATE_KEY_FILE" \
        -o UserKnownHostsFile=/dev/null \
        -o StrictHostKeyChecking=no \
        "$REMOTE_USER@127.0.0.1" \
        -p $LOCAL_PORT \
        "netstat -tpln 2> /dev/null | grep $REMOTE_PORT" &> /dev/null
    
    # If we get a error status, we proceed to create the connection
    if [ "$?" != 0 ]; then
        #  If the past command fails, first we'll search for the ssh process running locally        
        #+ and then kill the possible hanged ssh process
        ssh_process=$(pgrep -f "ssh.*$LOCAL_PORT")
        [ -n "$ssh_process" ] && kill "$ssh_process" &> /dev/null

        #  After we kill the hanged ssh process, we restore the ssh connection to our bastion.
        #+ This time we'll add the parameters:
        #+  -f, for leaving the process running in the background
        #+  -N, for avoiding start a shell
        #+  -R, to establish the remote port forwarding of our shell to the remote node
        #+      running in the remote port
        #+  -L, to establish the local forwarding by running the remote shell in our local machine
        ssh \
            -i "$PACKED_PRIVATE_KEY_FILE" \
            -o UserKnownHostsFile=/dev/null \
            -o StrictHostKeyChecking=no \
            -fN \
            -R "$REMOTE_PORT:127.0.0.1:22" \
            -L "$LOCAL_PORT:$REMOTE_HOST:22" \
            "$REMOTE_NODE" &> /dev/null
    fi
}

check_key
check_and_create_tunnel

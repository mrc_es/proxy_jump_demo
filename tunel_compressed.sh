#!/usr/bin/env bash

#[START Config]
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin # For cronjob environment
DIRNAME=$( dirname "$( realpath -s "$0" )" )

REMOTE_USER="bastion"
REMOTE_HOST="<bastion ip>"
REMOTE_NODE="$REMOTE_USER@$REMOTE_HOST"

REMOTE_PORT=10001
LOCAL_PORT=19922

PACKED_PRIVATE_KEY_FILE="$DIRNAME/credentials/bastion_id_rsa"
#[END Config]

check_and_create_tunnel() {
    ssh \
        -i "$PACKED_PRIVATE_KEY_FILE" \
        -o UserKnownHostsFile=/dev/null \
        -o StrictHostKeyChecking=no \
        "$REMOTE_USER@127.0.0.1" \
        -p $LOCAL_PORT \
        "netstat -tpln 2> /dev/null | grep $REMOTE_PORT" &> /dev/null
    
    if [ "$?" != 0 ]; then
        ssh_process=$(pgrep -f "ssh.*$LOCAL_PORT")
        [ -n "$ssh_process" ] && kill "$ssh_process" &> /dev/null
        ssh \
            -i "$PACKED_PRIVATE_KEY_FILE" \
            -o UserKnownHostsFile=/dev/null \
            -o StrictHostKeyChecking=no \
            -fN \
            -R "$REMOTE_PORT:127.0.0.1:22" \
            -L "$LOCAL_PORT:$REMOTE_HOST:22" \
            "$REMOTE_NODE" &> /dev/null
    fi
}

check_and_create_tunnel
